
/*
 * This program does a lot of thing
 *
 * The basic algorithm is to store the win combinations
 * such as in how many ways a player can win.
 * And an array that stores which button is pressed by which player
 *
 * if a player 1 clicked button 4(index 3) then we will assign
 * value 1 in index 3 such as clickedButton[3] = 1
 *
 * same for player 2 we will insert value 2
 *
 * So here is the action. When any player click on any button we will
 * check who clicked the button and update our clickedButton array
 * then we will check whether we got a win after clicking a button
 * Here we have to traverse every item in the wincomb array and
 * compare it with clickedButton array as which player wins th game
 *
*/

#include "widget.h"
#include "ui_widget.h"

#include <QDebug>

Widget::Widget(QWidget *parent)
	: QWidget(parent)
	, ui(new Ui::Widget)
{
	ui->setupUi(this);

	list = {
		ui->btn1, ui->btn2, ui->btn3,
		ui->btn4, ui->btn5, ui->btn6,
		ui->btn7, ui->btn8, ui->btn9
	};

	ui->playAgain->setVisible(false);

	// Connecting the signal and slot manually
	connect(ui->btn1, &QPushButton::clicked, this, &Widget::btn1Clicked);
	connect(ui->btn2, &QPushButton::clicked, this, &Widget::btn2Clicked);
	connect(ui->btn3, &QPushButton::clicked, this, &Widget::btn3Clicked);
	connect(ui->btn4, &QPushButton::clicked, this, &Widget::btn4Clicked);
	connect(ui->btn5, &QPushButton::clicked, this, &Widget::btn5Clicked);
	connect(ui->btn6, &QPushButton::clicked, this, &Widget::btn6Clicked);
	connect(ui->btn7, &QPushButton::clicked, this, &Widget::btn7Clicked);
	connect(ui->btn8, &QPushButton::clicked, this, &Widget::btn8Clicked);
	connect(ui->btn9, &QPushButton::clicked, this, &Widget::btn9Clicked);
}

Widget::~Widget()
{
	delete ui;
}

// This function is used to simplify all the button clicks
// In each and every 9 buttons we have . All of them will contain same code
// So for not writting duplicate code we created our own function
void Widget::buttonClicked(int index, QPushButton *btn) // Provide the button index and button pointer
{
	// Get which player clicked the button based on button index
	int player = playerTurn(index);

	if (player == 1) {
		btn->setText("x");    // player one then set the button text to 'x'
	} else if (player == 2) {
		btn->setText("o");    // player two then set the button text to 'o'
	}

	btn->setDisabled(true); // Set the button disable so that player can't click it again

	bool someoneWon = true;
	QString wonMessage = "";

	int winner = checkWinner(clickedButton);
	if (winner == 0) {
		wonMessage = "Game Tied";
	} else if (winner == 1 || winner == 2) {
		wonMessage = QString("Player %1 won!!!").arg(winner);
	} else {
		someoneWon = false;
	}

	if (someoneWon) {
		ui->status->setText(wonMessage);
		ui->player->setText("Game finished");

		for (auto btn : list) {
			btn->setDisabled(true);
		}

		ui->player->setText("");
		ui->playAgain->setVisible(true);
	}

	if (player == 1 && !someoneWon) {
		computerTurn();
	}
}

int Widget::checkWinner(int buttonStates[9])
{
	bool someoneWon = false;
	int playerWon = -1; // Nothing happened

	// Check all the possible combinations at the wincomb array
	for (int i = 0; i < 8; i++) {
		// Get each of the win button index
		int btnIndex1 = wincombs[i][0];
		int btnIndex2 = wincombs[i][1];
		int btnIndex3 = wincombs[i][2];

		// Match all the clicked buttons with our wincombination array
		if (
			buttonStates[btnIndex1] == 1 &&
			buttonStates[btnIndex2] == 1 &&
			buttonStates[btnIndex3] == 1
		) {
			// Check if the value is 1 then player 1 wins
			playerWon = 1;
			someoneWon = true;
		} else if (
			buttonStates[btnIndex1] == 2 &&
			buttonStates[btnIndex2] == 2 &&
			buttonStates[btnIndex3] == 2
		) {
			// If index is 2 player 2 wins
			playerWon = 2;
			someoneWon = true;
		}

		if (someoneWon) {
			break;
		}
	}

	// Check for tie (Doing an extra check)
	if (!someoneWon && checkTie()) {
		playerWon = 0;
	}

	return playerWon;
}

bool Widget::checkTie()
{
	for (int i = 0; i < 9; i++) {
		if (clickedButton[i] == 0) {
			return false;
		}
	}

	return true;
}

void Widget::computerTurn()
{
	int move = 0;
	int bestScore = INT_MIN;

	for (int i = 0; i < 9; i++) {
		if (clickedButton[i] == 0) {
			clickedButton[i] = 2;
			int score = miniMax(clickedButton, 0, false);
			clickedButton[i] = 0;
			if (score > bestScore) {
				bestScore = score;
				move = i;
			}
		}
	}

	if (move >= 0 && move <= 8) {
		QPushButton *btn = this->findChild<QPushButton*>(QString("btn%1").arg(move + 1));
		if (btn) btn->click();
	} else {
		qWarning() << "Invalid move" << move;
	}
}

int Widget::miniMax(int buttonStates[9], int depth, bool isMaximizing)
{
	int player = checkWinner(buttonStates);
	if (player != -1) {
		return lookupTable[player];
	}

	int bestScore;

	if (isMaximizing) {
		bestScore = INT_MIN;
		for (int i = 0; i < 9; i++) {
			if (clickedButton[i] == 0) {
				clickedButton[i] = 2;
				int score = miniMax(buttonStates, depth + 1, false);
				clickedButton[i] = 0;
				bestScore = std::max(score, bestScore);
			}
		}

		return bestScore;
	} else {
		bestScore = INT_MAX;
		for (int i = 0; i < 9; i++) {
			if (clickedButton[i] == 0) {
				clickedButton[i] = 1;
				int score = miniMax(buttonStates, depth + 1, true);
				clickedButton[i] = 0;
				bestScore = std::min(score, bestScore);
			}
		}

		return bestScore;
	}
}

void Widget::btn1Clicked()
{
	// Call button clicked with the button index and pointer
	buttonClicked(0, ui->btn1);
}

void Widget::btn2Clicked()
{
	buttonClicked(1, ui->btn2);
}

void Widget::btn3Clicked()
{
	buttonClicked(2, ui->btn3);
}

void Widget::btn4Clicked()
{
	buttonClicked(3, ui->btn4);
}

void Widget::btn5Clicked()
{
	buttonClicked(4, ui->btn5);
}

void Widget::btn6Clicked()
{
	buttonClicked(5, ui->btn6);
}

void Widget::btn7Clicked()
{
	buttonClicked(6, ui->btn7);
}

void Widget::btn8Clicked()
{
	buttonClicked(7, ui->btn8);
}

void Widget::btn9Clicked()
{
	buttonClicked(8, ui->btn9);
}


// Get which player turn it is
int Widget::playerTurn(int buttonIndex)
{
	// If the clicked button index is zero no one clicked it
	if (clickedButton[buttonIndex] == 0) {
		// increase how many buttons is clicked
		counter++;

		// Check the counter whether it is even or odd
		// even meant player 2 clicked the button
		// odd meant player 1 clicked the button
		int mod = counter % 2; // Modulus '%' used to determine the number is even or odd

		if (mod == 1) {
//			board[buttonIndex / 3][buttonIndex % 3] = "x";
			clickedButton[buttonIndex] = 1; // Set the index at the clicked button array
			ui->player->setText("Player 2");
			return 1;
		} else {
//			board[buttonIndex / 3][buttonIndex % 3] = "o";
			clickedButton[buttonIndex] = 2;
			ui->player->setText("Player 1");
			return 2;
		}
	}

	return 0;
}

void Widget::on_playAgain_clicked()
{
	for (auto btn : list) {
		btn->setEnabled(true);
		btn->setText("");
	}

	int k = 0;
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			clickedButton[k++] = 0;
//			board[i][j] = "";
		}
	}

	counter = 0;

	ui->player->setText("Player 1");
	ui->status->setText("Turn");
	ui->playAgain->setVisible(false);
}
