# TicTacToe

Play [tictactoe](https://s96abrar.gitlab.io/tictactoe) with computer.

This project written in C++ with Qt Framework 5.15.2 with the help of [Emscripten](https://emscripten.org/index.html) to create webassembly code for Qt.

For further reading 
- [Emscripten setup](https://emscripten.org/docs/getting_started/downloads.html)
- [Qt for WebAssembly](https://doc.qt.io/qt-5/wasm.html)

## Setting up the environment to build

### 1. Install Emscripten

Download the source code from github -

```sh
# Get the emsdk repo
git clone https://github.com/emscripten-core/emsdk.git

# Enter that directory
cd emsdk

# Fetch the latest version of the emsdk (not needed the first time you clone)
git pull
```

Setup Emscripten for Qt -
```sh
# Download and install the latest SDK tools.
# Must use the specific version 1.39.8 which is compatible with Qt 5.15
./emsdk install 1.39.8

# Make the "latest" SDK "active" for the current user. (writes .emscripten file)
./emsdk activate --embedded 1.39.8

# Activate PATH and other environment variables in the current terminal
# Always need (if not added to environement) to run when using emscripten tools.
source ./emsdk_env.sh
```

### 2. Use Qt WebAssembly

For using Qt webassebly we must need the online installer to install the Qt 5.15.2 WebAssembly module.

### 3. Build with `./compile`

Clone the project - 
```sh
git clone https://gitlab.com/s96Abrar/tictactoe tictactoe_webassembly
cd tictactoe_webassembly
```

Setup the environment variables for emscripten - 
```sh
source <Path-of-the-emsdk-folder>/emsdk_env.sh
```

Run `./compile` - 
```sh
./compile <Path-of-the-qmake-of-webassembly>
```

**N.B. `Path-of-the-qmake-of-webassembly` qmake binary path where Qt online installer installed the WebAssembly. Example path `/home/<user>/Qt/5.15.2/wasm_32/bin/qmake`**
