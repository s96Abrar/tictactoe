#pragma once // This nothing just the same as statictical calc application  header file


// Same as statisticalcalc app
// More comments wirtten in there
// All things are same as that


#include <QWidget>
#include <QPushButton>

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
	Q_OBJECT

public:
	Widget(QWidget *parent = nullptr);
	~Widget();

private slots:
	void btn1Clicked();
	void btn2Clicked();
	void btn3Clicked();
	void btn4Clicked();
	void btn5Clicked();
	void btn6Clicked();
	void btn7Clicked();
	void btn8Clicked();
	void btn9Clicked();

	void on_playAgain_clicked();

private:
	Ui::Widget *ui;

	// A counter that keeps track of how many click happeded
	int counter = 0;
	// An array which button is been clicked
	// You can get more info in the slides
	int clickedButton[9] = {0, 0, 0, 0, 0, 0, 0, 0, 0};

	// A win combinations based on the index of the buttons
	int wincombs[8][3] = {
		{0,1,2},
		{0,3,6},
		{6,7,8},
		{2,5,8},
		{0,4,8},
		{2,4,6},
		{1,4,7},
		{3,4,5}
	};

//	QStringList board[3] = {
//		{"", "", ""},
//		{"", "", ""},
//		{"", "", ""}
//	};

	int lookupTable[3] = {0, -1, 1};

	QList<QPushButton*> list;

	void buttonClicked(int index, QPushButton *btn);
	int playerTurn(int buttonIndex);
	void computerTurn();
	int miniMax(int buttonStates[], int depth, bool isMaximizing);
	int checkWinner(int buttonStates[]);
	bool checkTie();
};
